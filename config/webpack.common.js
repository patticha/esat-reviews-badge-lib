const paths = require('./paths');
const path = require('path');
const webpack = require('webpack');

const entry =
    process.env.NODE_ENV === 'production'
        ? path.resolve(__dirname, '../src/entry.js')
        : path.resolve(__dirname, '../src/entry-dev.js');

const filename =
    process.env.NODE_ENV === 'production'
        ? process.env.NODE_MODE === 'debug' ? 'js/integration.js' : 'js/integration.min.js'
        : 'js/integration.dev.js';

let host, cdn;
switch (process.env.NODE_ENV) {
    case 'production':
        host = paths.hosts.PRODUCTION;
        cdn = paths.cdn.PRODUCTION;
        break;
    case 'development':
        host = paths.hosts.DEVELOPMENT;
        cdn = paths.cdn.DEVELOPMENT;
        break;
    case 'qa':
        host = paths.hosts.QA;
        cdn = paths.cdn.QA;
        break;
    default:
        host = paths.hosts.PRODUCTION;
        cdn = paths.cdn.PRODUCTION;
}

const DEPLOYMENT_PATH = 'clients/products/badge/v1.0';
const DIST_PATH = 'dist';

module.exports = {
    paths: {
        entry,
        public: path.resolve(__dirname, `../${DIST_PATH}`),
        src: path.resolve(__dirname, '../src'),
        publicPath: `${cdn}/${DEPLOYMENT_PATH}/`
    },
    output: {
        filename
    },
    module: {
        rules: [
            {
                test: /\.s?[ac]ss$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader', options: {sourceMap: true}},
                    {loader: 'sass-loader', options: {sourceMap: true}}
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'images'
                }
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                loader: 'url-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'fonts'
                }
            },
            {
                test: /\.(html)$/,
                use: {
                    loader: 'html-loader',
                    options: {
                        attrs: [':data-src']
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            PRODUCTION: process.env.NODE_ENV === 'production',
            DEBUG: process.env.NODE_ENV === 'development' || process.env.NODE_MODE === 'debug',
            HOST: JSON.stringify(host),
            CDN: JSON.stringify(cdn)
        })
    ]
};

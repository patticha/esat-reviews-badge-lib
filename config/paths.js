module.exports = {
    cdn: {
        PRODUCTION: 'https://cdn.e-satisfaction.com',
        DEVELOPMENT: 'http://cdn.e-satisfaction.localhost:8080/demo',
        QA: 'http://cdn.qa.e-satisfaction.com'
    },
    hosts: {
        PRODUCTION: 'https://reviews.e-satisfaction.com',
        DEVELOPMENT: 'http://reviews.e-satisfaction.localhost:8080/demo',
        QA: 'http://reviews.qa.e-satisfaction.com'
    }
};

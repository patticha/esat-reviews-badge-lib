const config = require('./webpack.common.js');

module.exports = {
    mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
    optimization: {
		minimize: process.env.NODE_MODE !== 'debug'
	},
    devtool: false,
    entry: config.paths.entry,
    output: {
        filename: config.output.filename,
        path: config.paths.public,
        publicPath: config.paths.publicPath
    },
    module: config.module,
    plugins: config.plugins
};

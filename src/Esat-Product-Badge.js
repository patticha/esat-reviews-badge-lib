import Fetch from './services/Fetch';
import ProductBanner from './components/Banner/ProductBanner';
import {utils} from './utils/utils';

class EsatProductBadge {
    constructor(esatProdBadge) {
        if (typeof esatProdBadge === 'undefined') {
            throw new Error('esat_pr_badge is undefined');
        }

        if (typeof esatProdBadge.application_id === 'undefined') {
            throw new Error('application_id is undefined');
        }

        if (typeof esatProdBadge.product_sku === 'undefined') {
            throw new Error('product_sku is undefined');
        }

        Object.assign(this, esatProdBadge);

        /**
         * Banner default configs
         */
        this.configurations = {
            active: false,
            active_mobile: false,
            theme: 'dark',
            display_reviews: false,
            receive_feedback: false,
            position: undefined,
            position_type: 'inside',
            displayFirst: 5,
            displayNext: 10,
            fixed_font: false
        };

        this.initialize();
    }

    get applicationId() {
        return this.application_id;
    }

    get isActive() {
        return this.configurations.active;
    }

    get isMobileActive() {
        return this.configurations.active_mobile;
    }

    get CSSSelector() {
        return this.configurations.position;
    }

    get esatSku() {
        return this.product_sku;
    }

    async initialize() {
        let configurations = await this.getConfig();
        this.configurations = {...this.configurations, ...configurations};

        if (!this.isActive) {
            return;
        }

        if (utils.isMobile() && !this.isMobileActive) {
            return;
        }

        // Checks every 1s
        let intervalId = setInterval(() => this.initializeBanner(intervalId), 1000);
    }

    /**
     *  Checking if CSS container exists and initialize the banner
     */
    initializeBanner(intervalId) {
        if (utils.isSelectorValid(this.CSSSelector)) {
            clearInterval(intervalId);
            const EsatProductBanner = new ProductBanner(
                this.configurations,
                this.esatSku
            );
            EsatProductBanner.initialize();
        }
    }

    /**
     *  Returns retailer's configuration
     *  @return {Object}
     */
    async getConfig() {
        try {
            let response = await Fetch.configuration(this.applicationId);
            const data = await response.data;
            return data;
        } catch (error) {
            console.error(error);
        }
    }
}

export default EsatProductBadge;

import Client from './Client';

class Http extends Client {
    constructor() {
        super();
    }

    /**
     * Performing a GET request
     *
     * @param {String} url
     * @param {Object} params
     */
    get(url, params) {
        const method = 'GET';
        return super.request(method, url, params);
    }

    /**
     * Performing a POST request
     *
     * @param {String} url
     * @param {Object} data
     */
    post(url, data) {
        const method = 'POST';
        return super.request(method, url, data);
    }
}

export default new Http();

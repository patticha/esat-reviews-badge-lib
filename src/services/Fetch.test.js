import Fetch from './Fetch';
import axios from 'axios';

jest.mock('axios');

beforeAll(() => {
    Fetch.cdn = 'https://cdn.e-satisfaction.com';
    Fetch.host = 'https://reviews.e-satisfaction.com';
});

afterEach(() => {
    axios.mockClear();
});

describe('Fetch', () => {

    test('Should throw an error if application ID is undefined ', () => {
        expect(() => {
            Fetch.configuration();
        }).toThrowError(new Error('[configuration]: applicationId is undefined'));

        expect(() => {
            Fetch.skuReviews();
        }).toThrowError(new Error('[skuReviews]: applicationId is undefined'));

        expect(() => {
            Fetch.reviews();
        }).toThrowError(new Error('[reviews]: applicationId is undefined'));

    });

    test('Should throw an error if sku is undefined', () => {
        expect(() => {
            Fetch.skuReviews('id');
        }).toThrowError(new Error('[skuReviews]: sku is undefined'));

    });

    test('Should catch the error', () => {
        axios.mockRejectedValue('API request failed');
        Fetch.configuration(80).catch(err => {
            expect(err).toEqual('Esat Client API request failed');
        });

    });

    test('Should get retailers configuration', () => {

        const expectToBeCallWith = {
            data: undefined,
            method: 'GET',
            params: {},
            url: 'https://cdn.e-satisfaction.com/files/app/80/reviews/config.json'

        };

        const  data = [
            {
                application_id: '80',
                active: true,
                active_mobile: true,
                theme: 'light',
                locale: 'en',
                display_reviews: true,
                receive_feedback: false,
                position: '.esat_review',
                position_type: 'inside',
                fixed_font:  false
            }
        ];
        axios.mockImplementation(() => Promise.resolve(data));

        Fetch.configuration(80);
        expect(axios).toHaveBeenCalledWith(expectToBeCallWith);
        expect(Fetch.configuration(80).then(data => expect(data).toEqual(data)));

    });

    test('Should get reviews for a given SKU', () => {
        let applicationId = 80;
        let sku  = 'SKU123123123';

        const expectToBeCallWith = {
            data: undefined,
            method: 'GET',
            params: {},
            url: `https://reviews.e-satisfaction.com/reviews/${applicationId}/${sku}`

        };
        const  data = [
            {
                title: 'Macbook PWEFDS',
                sku: 'SKU123123123',
                esat_sku: 'ESAT_SKU123123123',
                image_url: 'https://cdn.e-satisfaction.com/path/to/logo.png',
                score: '4.9',
                max_score: '5',
                total_reviews_count: '88',
                scores: [
                    {
                        title: 'Display 4th',
                        contents: {
                            el: ' ',
                            en: 'Total Review'
                        },
                        display_order: 4,
                        score: '2.5',
                        max_score: '5'
                    }
                ]
            }
        ];

        axios.mockImplementation(() => Promise.resolve(data));
        Fetch.skuReviews(applicationId, sku);
        expect(axios).toHaveBeenCalledWith(expectToBeCallWith);
    });

    test('Should get all reviews for a given retailer', () => {
        let applicationId = 80;

        const expectToBeCallWith = {
            data: undefined,
            method: 'GET',
            params: {},
            url: `https://reviews.e-satisfaction.com/reviews/${applicationId}`

        };
        const  data = [
            {
                title: 'Macbook PWEFDS',
                sku: 'SKU123123123',
                esat_sku: 'ESAT_SKU123123123',
                image_url: 'https://cdn.e-satisfaction.com/path/to/logo.png',
                score: '4.9',
                max_score: '5',
                total_reviews_count: '88',
                scores: [
                    {
                        title: 'Display 4th',
                        contents: {
                            el: ' ',
                            en: 'Total Review'
                        },
                        display_order: 4,
                        score: '2.5',
                        max_score: '5'
                    }
                ]
            }
        ];

        axios.mockImplementation(() => Promise.resolve(data));
        Fetch.reviews(applicationId);
        expect(axios).toHaveBeenCalledWith(expectToBeCallWith);
    });
});

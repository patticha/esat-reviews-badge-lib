import Http from './http/Http';

class Fetch {
    constructor() {
        if (typeof HOST === 'undefined') {
            return;
        }

        this.host = HOST;
        this.cdn = CDN;
    }

    /**
     * Gets retailer's configuration
     *
     * @param {Number} applicationId
     */
    configuration(applicationId) {
        if (typeof applicationId === 'undefined') {
            throw new Error('[configuration]: applicationId is undefined');
        }

        const url = `${this.cdn}/files/app/${applicationId}/reviews/config.json`;

        return Http.get(url);
    }

    /**
     * Get reviews for a given SKU.
     *
     * @param {Number} applicationId
     * @param {String} sku
     */
    skuReviews(applicationId, sku) {
        if (typeof applicationId === 'undefined') {
            throw new Error('[skuReviews]: applicationId is undefined');
        }

        if (typeof sku === 'undefined') {
            throw new Error('[skuReviews]: sku is undefined');
        }

        const url = `${this.host}/reviews/${applicationId}/${sku}`;

        return Http.get(url);
    }

    /**
     * Get all reviews for a given retailer.
     *
     * @param {Number} applicationId
     */
    reviews(applicationId) {
        if (typeof applicationId === 'undefined') {
            throw new Error('[reviews]: applicationId is undefined');
        }

        const url = `${this.host}/reviews/${applicationId}`;

        return Http.get(url);
    }
}

export default new Fetch();

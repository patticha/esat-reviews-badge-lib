/* eslint-disable max-len */
import {translate} from './translate';

test('Should replace english data-translate values', () => {
    document.body.innerHTML =
  `<div class="esat">
    <span data-translate="from"/>
    <span data-translate="totalReviews"/>
  </div>`;
    let nodes = document.querySelectorAll('.esat [data-translate]');
    translate.all('en-us', nodes);

    const [from, total] = nodes;
    expect(from.innerText).toBe('from');
    expect(total.innerText).toBe('Reviews');
});

test('Should replace greek data-translate values', () => {
    document.body.innerHTML =
  `<div class="esat">
    <span data-translate="from"/>
    <span data-translate="totalReviews"/>
  </div>`;
    let nodes = document.querySelectorAll('.esat [data-translate]');
    translate.all('el', nodes);

    const [from, total] = nodes;
    expect(from.innerText).toBe('απο');
    expect(total.innerText).toBe('αξιολογήσεις αγοραστών');
});

test('Should return the translation for the given key', () => {
    let key = 'noComment';
    expect(translate.get('el', key)).toBe('Ο αξιολογητής δεν άφησε κανένα σχόλιο');

    key = 'loadMore';
    expect(translate.get('en-us', key)).toBe('Load More');

    key = 'doesntExist';
    expect(translate.get('en-us', key)).toBe(undefined);

});

test('Should return the default translation for invalid local', () => {
    let key = 'noComment';
    expect(translate.get('invalid', key)).toBe('Ο αξιολογητής δεν άφησε κανένα σχόλιο');

    key = 'loadMore';
    expect(translate.get('invalid', key)).toBe('Φορτωσε περισσοτερα');
});

test('Should get the local',  () => {
    let locale = 'en';
    expect(translate.getLocale(locale)).toBe(locale);
});

test('Should return browsers locale',  () => {
    let myLocale = 'en';

    Object.defineProperty(window.navigator, 'language', {value: myLocale, configurable: true});
    expect(translate.getLocale()).toBe(myLocale);
});

test('Should return default if users language is not supported',  () => {
    let myLocale = 'ru';

    Object.defineProperty(window.navigator, 'language', {value: myLocale, configurable: true});
    expect(translate.getUserLocale()).toBe(translate.DEFAULT);
});

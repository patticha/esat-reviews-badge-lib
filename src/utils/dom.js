export const dom = {
    BANNER_CLASS: '.esat-pr-banner',

    addClass: (element, className) => {
        if (element.classList) {
            element.classList.add(className);
        } else {
            element.className += ' ' + className;
        }
    },

    removeClass: (element, className) => {
        if (element.classList) {
            element.classList.remove(className);
        } else {
            element.className.replace(className, ' ');
        }
    },

    removeClassFromList: (nodes, className) => {
        for (let node of nodes) {
            dom.removeClass(node, className);
        }
    },

    toggleClass: (element, className) => {
        element.classList.toggle(className);
    },

    toggleClassConditionally: (element, className, condition) => {
        condition
            ? dom.addClass(element, className)
            : dom.removeClass(element, className);
    },

    applyTheme: (element, theme) => {
        const className = `${element.classList[0]}--${theme}`;
        dom.addClass(element, className);
    },

    applyMobileLayout: (parent) => {
        let banner = document.querySelector(dom.BANNER_CLASS);

        const mobileClass = banner ? `${banner.classList[0]}--mobile` : '';

        dom.toggleClassConditionally(
            banner,
            mobileClass,
            dom.isSmallContainer(parent)
        );
    },

    hideElement: (element) => {
        dom.addClass(element, 'esat-pr-banner-elem--hidden');
    },

    showElement: (element) => {
        dom.removeClass(element, 'esat-pr-banner-elem--hidden');
    },

    isSmallContainer: (element) => {
        return element.offsetWidth <= 500;
    },

    isStarDisabled: (element) => {
        return element.classList.contains(
            'esat-pr-banner__header-stats-stars-bar-flex--disabled'
        );
    },

    setStyle: (element, prop, value) => {
        element.style[prop] = value;
    },

    /**
     * Parses the given template as HTML and inserts the this tree
     * into the given position
     *
     * @param {Object} parent
     * @param {String} position
     * @param {Object} template
     */
    appendComponent: (parent, position = 'afterbegin', template) => {
        if (!parent) {
            return;
        }

        parent.insertAdjacentHTML(
            position,
            template
        );
    },

    /**
     * Sanitize given value to avoid XSS
     *
     * @param {String} value
     * @retun {String}
     */
    sanitizeHTML: (value) => {
        if (typeof value === 'string') {
            return value.replace(
                /(\b)(on\S+)(\s*)=|<iframe|javascript|(<\s*)(\/*)script/gi,
                '\\$&\\'
            );
        }

        return value;
    },

    /**
     * Sets the HTML within the given element
     *
     * @param {Object} element
     * @param {Object} html
     */
    updateHTML: (element, html) => {
        if (!element) {
            return;
        }

        let value = html ? html : '-';
        if (element.length) {
            for (const el of element) {
                el.innerHTML = value;
            }
        } else {
            element.innerHTML = value;
        }
    },

    /**
     * Replace template variables
     *
     * @param {String} template
     * @param {Object} templateVars
     */
    templateEngine: (template, templateVars) => {
        let parsed = template;
        Object.keys(templateVars).forEach(
            (key) => {
                const value = dom.sanitizeHTML(templateVars[key]);
                parsed = parsed.replace('${' + key + '}', value);
            }
        );
        return parsed;
    }
};

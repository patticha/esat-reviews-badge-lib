import lang from '../locale.json';

export const translate = {

    DEFAULT: 'el',

    /**
     *  Renders the translated text content of the given list of nodes
     * @param {String} locale
     * @param {Array} nodes
     */
    all: (locale, nodes) => {
        for (let node of nodes) {
            node.innerText = translate.get(locale, node.dataset.translate);
        }
    },

    /**
     *  Returns the translation for the given key
     * @param {String} locale
     * @param {String} key
     * @return {String}
     */
    get: (locale, key) => {
        let supportedTranslation = Object.keys(lang);

        return supportedTranslation.includes(locale)
            ? lang[locale][key]
            : lang[translate.DEFAULT][key];

    },

    /**
     * Returns locale
     * @param {String} locale
     * @return {String}
     */
    getLocale: (locale) => {

        if (locale) { return locale; }

        return translate.getUserLocale();
    },

    /**
     * Detects user's locale. if not supported returns the default one
     * @return {String}
     */
    getUserLocale: () => {
        const languages = ['en-us', 'en', 'el', 'es', 'ca', 'ro'];

        const userLocale = navigator.language.toLocaleLowerCase();

        return languages.includes(userLocale) ? userLocale : translate.DEFAULT;
    }
};

/* eslint-disable max-len */
import {dom} from './dom';

test('Should add class', () => {
    const classes = { 0: 'esat', 1: 'added-class'};
    document.body.innerHTML = '<div> <span class="esat"/> </div>';
    const elem = document.querySelector('.esat');

    dom.addClass(elem, 'added-class');

    expect(elem.classList).toMatchObject(classes);
    expect(elem.classList.length).toBe(2);

});

test('Should remove class', () => {
    const classes = { 0: 'esat'};
    document.body.innerHTML = '<span class="esat remove-this-class"/>';
    const elem = document.querySelector('.esat');

    dom.removeClass(elem, 'remove-this-class');
    expect(elem.classList).toMatchObject(classes);
    expect(elem.classList.length).toBe(1);

});

test('Should remove class from a list of nodes', () => {
    const classes = { 0: 'esat'};
    document.body.innerHTML = '<span class="esat remove-this-class"</span> <span class="esat remove-this-class"</span>';
    const elements = document.querySelectorAll('.esat');

    dom.removeClassFromList(elements, 'remove-this-class');
    const [first, second] = elements;

    expect(first.classList).toMatchObject(classes);
    expect(first.classList.length).toBe(1);

    expect(second.classList).toMatchObject(classes);
    expect(first.classList.length).toBe(1);

});

test('Should apply light theme class', () => {
    const classes = { 0: 'esat', 1: 'esat--light'};
    document.body.innerHTML = '<span class="esat"/>';
    const elem = document.querySelector('.esat');

    dom.applyTheme(elem, 'light');

    expect(elem.classList).toMatchObject(classes);
    expect(elem.classList.length).toBe(2);

});

test('Should apply dark theme class', () => {
    const classes = { 0: 'esat', 1: 'esat--dark'};
    document.body.innerHTML = '<span class="esat"/>';
    const elem = document.querySelector('.esat');

    dom.applyTheme(elem, 'dark');

    expect(elem.classList).toMatchObject(classes);
    expect(elem.classList.length).toBe(2);

});

test('Should apply mobile class', () => {
    const classes = { 0: 'esat-pr-banner'};
    Object.defineProperty(HTMLElement.prototype, 'offsetWidth', { configurable: true, value: 300 });

    document.body.innerHTML = ` <div class="parent" style="width:1000px"> 
                                    <div class="esat-pr-banner"> </div>
                                <div/>`;

    const parent = document.querySelector('.parent');
    const banner = document.querySelector('.esat-pr-banner');

    dom.applyMobileLayout(parent);
    expect(banner.classList[1]).toBe('esat-pr-banner--mobile');
    expect(banner.classList.length).toBe(2);

    Object.defineProperty(HTMLElement.prototype, 'offsetWidth', { configurable: true, value: 501 });

    dom.applyMobileLayout(parent);
    expect(banner.classList).toMatchObject(classes);
    expect(banner.classList.length).toBe(1);

});

test('Should apply class to hide element', () => {
    const classes = { 0: 'esat', 1: 'esat-pr-banner-elem--hidden'};
    document.body.innerHTML = '<span class="esat"/>';
    const elem = document.querySelector('.esat');

    dom.hideElement(elem);
    expect(elem.classList).toMatchObject(classes);
    expect(elem.classList.length).toBe(2);

});

test('Should remove hide class to show element', () => {
    const classes = { 0: 'esat'};
    document.body.innerHTML = '<span class="esat esat-pr-banner-elem--hidden"/>';
    const elem = document.querySelector('.esat');

    dom.showElement(elem);
    expect(elem.classList).toMatchObject(classes);
    expect(elem.classList.length).toBe(1);

});

test('Should toggle class', () => {
    let classes = { 0: 'esat'};
    document.body.innerHTML = '<span class="esat visible"/>';
    const elem = document.querySelector('.esat');

    dom.toggleClass(elem, 'visible');
    expect(elem.classList).toMatchObject(classes);
});

test('Should toggle class conditionally - add class', () => {
    let classes = { 0: 'esat', 1: 'visible'};
    document.body.innerHTML = '<span class="esat "/>';
    const elem = document.querySelector('.esat');

    dom.toggleClassConditionally(elem, 'visible', true);
    expect(elem.classList).toMatchObject(classes);
});

test('Should toggle class conditionally - remove class', () => {
    let classes = { 0: 'esat'};
    document.body.innerHTML = '<span class="esat visible"/>';
    const elem = document.querySelector('.esat');

    dom.toggleClassConditionally(elem, 'visible', false);
    expect(elem.classList).toMatchObject(classes);
});

test('Should return true if the element is < 500px', () => {
    Object.defineProperty(HTMLElement.prototype, 'offsetWidth', { configurable: true, value: 499 });

    document.body.innerHTML = '<span class="esat "/>';
    const elem = document.querySelector('.esat');

    let boolean = dom.isSmallContainer(elem);
    expect(boolean).toBeTruthy();
});

test('Should return false if the element is > 500px', () => {
    Object.defineProperty(HTMLElement.prototype, 'offsetWidth', { configurable: true, value: 501 });

    document.body.innerHTML = '<span class="esat "/>';
    const elem = document.querySelector('.esat');

    let boolean = dom.isSmallContainer(elem);
    expect(boolean).not.toBeTruthy();
});

test('Should set style property to the element', () => {

    document.body.innerHTML = '<span class="esat "/>';
    const elem = document.querySelector('.esat');

    const property = 'color';
    const value = 'red';

    dom.setStyle(elem, property, value);
    expect(elem.style[property]).toBe(value);
});

test('Should check if star is disabled based on the class', () => {
    document.body.innerHTML = '<span class="esat esat-pr-banner__header-stats-stars-bar-flex--disabled"/>';
    let elem = document.querySelector('.esat');

    expect(dom.isStarDisabled(elem)).toBeTruthy();

    document.body.innerHTML = '<span class="esat"/>';
    elem = document.querySelector('.esat');

    expect(dom.isStarDisabled(elem)).toBeFalsy();

});

test('Should exit appendComponent if parent is not available', () => {
    document.body.innerHTML = '<div class="parent" > <span class="child"></span> </div>';
    let parent = document.querySelector('.invalid-selector');
    const template = '<div> the template </div>';
    dom.appendComponent(parent, 'beforebegin', template);

    let initialTemplate = '<div class="parent"> <span class="child"></span> </div>';
    expect(document.body.innerHTML).toEqual(initialTemplate);
});

test('Should append html template before the element', () => {
    document.body.innerHTML = '<div class="parent" > <span class="child"/> </div>';
    let parent = document.querySelector('.parent');
    const template = '<div> the template </div>';
    dom.appendComponent(parent, 'beforebegin', template);

    let renderedHtml = '<div> the template </div><div class="parent"> <span class="child"> </span></div>';
    expect(document.body.innerHTML).toEqual(renderedHtml);
});

test('Should append html template after the element', () => {
    document.body.innerHTML = '<div class="parent" > <span class="child"/> </div>';
    let parent = document.querySelector('.parent');
    const template = '<div> the template </div>';
    dom.appendComponent(parent, 'afterbegin', template);

    let renderedHtml = '<div class="parent"><div> the template </div> <span class="child"> </span></div>';
    expect(document.body.innerHTML).toBe(renderedHtml);
});

test('Should exit updateHTML if elemement is not available', () => {
    document.body.innerHTML = '<div class="parent"> </div>';
    let elem = document.querySelector('.invalid');

    dom.updateHTML(elem, '<p>test</p>');
    let initialTemplate = '<div class="parent"> </div>';

    expect(document.body.innerHTML).toEqual(initialTemplate);
});

test('Should update the inner HTML of the element', () => {
    document.body.innerHTML = '<div class="parent"> </div>';
    let elem = document.querySelector('.parent');

    dom.updateHTML(elem, '<p>test</p>');
    let renderedHtml = '<div class="parent"><p>test</p></div>';

    expect(document.body.innerHTML).toEqual(renderedHtml);
});

test('Should update the inner HTML of the all the elements', () => {
    document.body.innerHTML = '<div class="elem"></div><div class="elem"> </div>';
    let elements = document.querySelectorAll('.elem');

    dom.updateHTML(elements, '<p>test</p>');
    let renderedHtml = '<div class="elem"><p>test</p></div><div class="elem"><p>test</p></div>';

    expect(document.body.innerHTML).toEqual(renderedHtml);
});

test('Should replace template variables', () => {
    const templateVars = {score: 100, max_score: 5, posive_comment: 'A positive comment'};
    const template = '<span>${score}</span><span>/${max_score}</span><span>${posive_comment}</span>';
    const expectedTemplate = '<span>100</span><span>/5</span><span>A positive comment</span>';

    expect(dom.templateEngine(template, templateVars)).toEqual(expectedTemplate);

});


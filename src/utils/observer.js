import {dom} from './dom';

export const observer = {
    resizeObserver: new ResizeObserver(entries => {
        for (let entry of entries) {
            if (entry.target) {
                dom.applyMobileLayout(entry.target);
            }
        }
    })
};

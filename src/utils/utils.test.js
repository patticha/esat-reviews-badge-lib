import {utils} from './utils';

test('Should return true when innerWidth <= 768', () => {
    Object.assign(window, { innerWidth: 767 });
    expect(utils.isMobile()).toBeTruthy();
});

test('Should check if css selector is valid', () => {
    const mySelector = '.esat-selector';
    document.body.innerHTML =
      `<div>
        <span class="${mySelector.replace('.', '')}" />
      </div>`;

    const isValid = utils.isSelectorValid(mySelector);
    expect(isValid).toBeTruthy();

    const isInvalid = utils.isSelectorValid('invalid-selector');
    expect(isInvalid).toBeFalsy();
});

test('Should return valid position for insertAdjacentHTML', () => {
    let positionType = 'inside';
    expect(utils.getPosition(positionType)).toBe('afterbegin');

    positionType = 'after';
    expect(utils.getPosition(positionType)).toBe('afterend');

    positionType = 'before';
    expect(utils.getPosition(positionType)).toBe('beforebegin');

    positionType = 'default';
    expect(utils.getPosition(positionType)).toBe('afterbegin');
});

test('Should return the correct date format', () => {
    const date = utils.getDate('01/11/2019', 'en');
    expect(date).toBe('11 January 2019');
});

test('Should return the percentage of a portion', () => {
    const total = 1000;
    const portion = 300;
    const percentage = utils.calculatePercentage(total, portion);
    expect(percentage).toBe('30.0');
});

test('Should return the percentage of a portion', () => {
    const total = 1000;
    const portion = 300;
    const percentage = utils.calculatePercentage(total, portion);
    expect(percentage).toBe('30.0');
});

test('Should group the data based on score', () => {

    const array = [
        {review: 1, score: 1},
        {review: 2, score: 2},
        {review: 3, score: 1},
        {review: 4, score: 2},
        {review: 5, score: 5}
    ];

    const expectedGroup = {
        1: [{ review: 1, score: 1 }, { review: 3, score: 1 }],
        2: [{ review: 2, score: 2 }, { review: 4, score: 2 }],
        5: [{ review: 5, score: 5 }]

    };
    expect(utils.groupData(array, 'score')).toStrictEqual(expectedGroup);
});

test('Should return sorted array', () => {
    const array = [[1, 2, 3], [1, 2, 3, 4]];
    let sort = utils.sort(array);
    const sortedArrayExpected = [[1, 2, 3, 4], [1, 2, 3]];
    expect(sort).toStrictEqual(sortedArrayExpected);

    sort = utils.sort('invalid-array');
    expect(sort).toStrictEqual([]);
});

export const utils = {
    /**
     * Determines if it's mobile
     *
     * @returns {Boolean}
     */
    isMobile: () => {
        return window.innerWidth <= 768;
    },

    /**
     * Checks if the given element is valid
     *
     * @params {Node} selector
     * @returns {Boolean}
     */
    isSelectorValid: (selector) => {
        const selectorAll = document.querySelectorAll(selector);
        return selectorAll.length > 0;
    },

    /**
     * Returns the valid position to be used for insertAdjacentHTML
     *
     * @params {String} positionType
     * @returns {String}
     */
    getPosition: (positionType) => {
        switch (positionType) {
        case 'inside':
            return 'afterbegin';
        case 'after':
            return 'afterend';
        case 'before':
            return 'beforebegin';
        default:
            return 'afterbegin';
        }
    },

    getDate: (date, locale) => {
        let d = new Date(date);
        let month = d.toLocaleString(locale, { month: 'long' });
        return `${d.getDate()} ${month} ${d.getFullYear()}`;
    },

    /**
     * Returns the percentage of a portion
     *
     * @param {Number} total
     * @param {Number} portion
     */
    calculatePercentage: (total, portion) => {
        return (Number(portion) / Number(total) * 100).toFixed(1);
    },

    /**
     * Groups an array of objects by the given groupBy property
     *
     * @param {Array} data
     * @param {String} groupBy
     * @returns {Object} Grouped object by groupBy value
     */
    groupData: (data, groupBy) => {
        const result = data.reduce((star, review) => {
            let key = review[groupBy];
            star[key] = star[key] || [];
            star[key].push(review);
            return star;
        }, {});

        return result;
    },

    /**
     * Sort array in descending order
     *
     * @param {Array} data
     * @returns {Array}
     */
    sort: (data) => {
        if (Array.isArray(data)) {
            return data.sort((a, b) => b.length - a.length);
        }

        return [];
    }
};

const getAssets = () =>
    import(/* webpackMode: 'eager' */  '../../../assets/css/banner.scss');
import {utils} from '../../utils/utils';
import {observer} from '../../utils/observer';
import {dom} from '../../utils/dom';
import {translate} from '../../utils/translate';
import Fetch from '../../services/Fetch';
import {View} from './productBannerView';

class ProductBanner {
    constructor(configurations, sku) {
        this.configurations = configurations;
        this.sku = sku;
        this.wrapper = '.esat-pr-banner';
        this.bannerHolder = '';
        this.data = {};
        this.elements = {};
        this.totalReviews = 0;
        this.starRating = {};
        this.filter = null;
        this.state = {
            reviews: []
        };
    }

    // Getters
    get MAX_SCORE() {
        return this.data.max_score;
    }

    get applicationId() {
        return this.configurations.application_id;
    }

    get theme() {
        return this.configurations.theme;
    }

    get position() {
        return this.configurations.position;
    }

    get locale() {
        return translate.getLocale(this.configurations.locale);
    }

    get displayReviews() {
        return this.configurations.display_reviews;
    }

    get positionType() {
        return this.configurations.position_type;
    }

    get displayFirst() {
        return this.configurations.displayFirst;
    }

    get displayNext() {
        return this.configurations.displayNext;
    }

    get hasFixedFont() {
        return this.configurations.fixed_font;
    }

    get hasReviews() {
        return this.data.total_reviews_count
            && this.data.total_reviews_count > 0 && this.hasScores;
    }

    get hasScores() {
        return this.data.scores && this.data.scores.length > 0;
    }

    /**
     *  Returns all reviews for the registered product and app
     *  @return {Object}
     */
    async getReviews() {
        try {
            let response = await Fetch.skuReviews(this.applicationId, this.sku);
            const data = await response.data;
            return data;

        } catch (error) {
            // Fail silently
            console.error(error);
        }
    }

    /**
     *  Initialize Banner template
     */
    async initialize() {
        await getAssets();

        this.bannerHolder = document.querySelector(this.position);
        observer.resizeObserver.observe(this.bannerHolder);

        // Get sku reviews
        let data = await this.getReviews();
        this.data = {...this.data, ...data};
        this.render();
    }

    /**
     *  Initialize Observer
     */
    initializeObserver() {
        const renderReviews = (reviews) => {
            View.renderReviews(
                reviews,
                this.elements.main.wrapper,
                this.MAX_SCORE,
                this.locale
            );
        };

        const createState = (state) => {
            return new Proxy(state, {
                set(target, property, value) {
                    target[property] = value;

                    // Update the reviews everytime the state changes
                    renderReviews(value);
                    return true;
                },
                get (target, prop) {
                    if (prop === 'reviews') {
                        return target;
                    }
                }
            });
        };

        // Initial state
        this.state = createState({reviews: []});
    }

    async render() {

        // Append banner into the page
        if (!this.hasReviews) {
            View.renderNoReviewsTemplate(
                this.bannerHolder,
                utils.getPosition(this.positionType)
            );
        } else {
            View.renderBannerTemplate(
                this.bannerHolder,
                utils.getPosition(this.positionType)
            );
        }

        let esatWrapper = document.querySelector(this.wrapper);

        // set fixed font-size
        if (this.hasFixedFont) {
            dom.setStyle(esatWrapper, 'fontSize', '16px');
        }

        dom.applyTheme(esatWrapper, this.theme);

        // Get all Banner Elements
        this.elements = {...View.getElements()};

        translate.all(this.locale, this.elements.translations.all);

        if (!this.hasReviews) {
            return;
        }

        this.starRating = utils.groupData(this.data.reviews, 'score');

        this.initializeObserver();
        this.loadBannerData();
        this.registerEventListeners();
    }

    loadBannerData() {
        // Update header
        this.loadHeader();

        if (this.displayReviews) {
            // Update Main content
            this.loadReviews(this.data.reviews);
        }
    }

    loadHeader() {
        const {header, common} = this.elements;
        const {scores, ...info} = this.data;
        const {score, total_reviews_count: totalReviewsCount} = info;
        const maxScore = `/${this.MAX_SCORE}`;

        dom.updateHTML(common.maxScore, maxScore);
        dom.updateHTML(header.totalScore, score);
        dom.updateHTML(header.totalReviews, totalReviewsCount);

        View.renderBarRating(
            scores,
            header.statsReviews,
            this.locale
        );

        const highestStarRating = utils.sort(Object.values(this.starRating));
        const totalReviewsNumber = highestStarRating.length || 0;

        // Stop rendering
        if (!totalReviewsNumber) {return;}

        // Star rating
        View.renderStarRating(
            this.starRating,
            header.starRating.wrapper,
            this.MAX_SCORE,
            totalReviewsNumber,
            this.displayReviews
        );

        this.elements = {...View.getElements()};

    }

    loadReviews(reviews) {
        // Render
        this.state.reviews = reviews.slice(0, this.displayFirst);

        this.totalReviews = reviews.length;
        this.elements = {...View.getElements()};

        if (this.totalReviews >= this.displayFirst) {
            dom.showElement(this.elements.footer.wrapper);
        } else {
            dom.hideElement(this.elements.footer.wrapper);
        }
    }

    loadMoreReviews() {
        const {footer} = this.elements;
        const currentDisplayed = this.state.reviews;
        let {reviews} = currentDisplayed;
        let currentTotalDisplayed = reviews.length;
        let next = currentTotalDisplayed + this.displayNext;
        let localReviews = this.filter
            ? this.starRating[this.filter]
            : this.data.reviews;

        let nextReviews = localReviews.slice(0, next);

        // Render
        this.state.reviews = nextReviews;

        if (currentTotalDisplayed >= this.totalReviews) {
            dom.hideElement(footer.wrapper);
        }
    }

    registerEventListeners() {
        this.onLoadMore();
        this.onStarFiltering();
    }

    onLoadMore() {
        let {footer} = this.elements;
        footer.loadMoreButton.addEventListener(
            'click',
            evt => this.loadMoreReviews(evt)
        );
    }

    filterReviews(starElem) {

        if (!this.displayReviews) {
            return;
        }

        if (dom.isStarDisabled(starElem.lastElementChild)) {
            return;
        }

        if (!View.isHighlighted(starElem)) {
            // Star
            this.filter = starElem.dataset.esatStar;
            View.highlightStarFilter(starElem);
            this.loadReviews(this.starRating[this.filter]);
        } else {
            this.filter = null;
            View.highlightStarFilter(starElem, true);

            // Reset reviews
            this.loadReviews(this.data.reviews);
        }
    }

    onStarFiltering() {
        let {header} = this.elements;
        const stars = header.starRating.ratings;

        for (const star of stars) {
            star.addEventListener(
                'click',
                // eslint-disable-next-line no-unused-vars
                evt => this.filterReviews(star)
            );
        }
    }
}

export default ProductBanner;

/* eslint-disable max-len */
import bannerTemplate from '../../templates/banner/banner.html';
import starRatingComponent from '../../templates/common/starRating.html';
import barRatingComponent from '../../templates/common/barRating.html';
import reviewComponent from '../../templates/banner/review.html';
import noReviewComponent from '../../templates/banner/noReviews.html';
import {translate} from '../../utils/translate';

import {dom} from '../../utils/dom';
import {utils} from '../../utils/utils';

/* eslint-disable camelcase */
export const View = {
    classes: {
        muted: 'esat-pr-banner__header-stats-stars-reviews-total--muted',
        highlighted: 'esat-pr-banner__header-stats-stars-rating--highlighted',
        mutedComment: 'esat-pr-banner__main-review-comment--muted'
    },

    getElements: () => {
        return {
            common: {
                maxScore: document.querySelectorAll('.esat-pr-banner__max-score')
            },
            header: {
                totalScore: document.querySelector('.esat-pr-banner__big-badge-score'),
                statsReviews: document.querySelector('.esat-pr-banner__header-stats-reviews-average'),
                totalReviews: document.querySelector('.esat-pr-banner-stats-total-reviews'),
                reviewBars: document.querySelector('.esat-pr-banner__header-stats-review-bar'),
                reviewValues: document.querySelector('.esat-pr-banner__header-stats-review-value'),
                starRating: {
                    wrapper: document.querySelector('.esat-pr-banner__header-stats-stars-reviews-total'),
                    ratings: document.querySelectorAll('.esat-pr-banner__header-stats-stars-rating'),
                    bars: document.querySelector('.esat-pr-banner__header-stats-stars-bar'),
                    reviewValue: document.querySelector('.esat-pr-banner__header-stats-stars-total')
                }
            },
            main: {
                wrapper: document.querySelector('.esat-pr-banner__main')
            },
            footer: {
                wrapper: document.querySelector('.esat-pr-banner__main-load-more'),
                loadMoreButton: document.querySelector('.esat-pr-banner__main-load-more-button')
            },
            translations: {
                all: document.querySelectorAll('.esat-pr-banner [data-translate]')
            }
        };
    },

    /**
     * Responsible for rendering banner template
     * @param {Node} element
     * @param {String} position
     */
    renderBannerTemplate: (element, position) => {
        dom.appendComponent(
            element,
            position,
            bannerTemplate
        );
    },

    /**
     * Responsible for rendering banner template
     * @param {Node} element
     * @param {String} position
     */
    renderNoReviewsTemplate: (element, position) => {
        dom.appendComponent(
            element,
            position,
            noReviewComponent
        );
    },

    /**
     * Parses barRating template
     * @param {Object} templateVariables
     * @return {String} parsed html
     */
    parseBarRatingComponent: (templateVariables) => {
        return dom.templateEngine(barRatingComponent, templateVariables);
    },

    /**
     * Responsible for displaying bar rating
     * @param {Array} scores
     * @param {Node} element
     * @param {String} locale
     * @param {String} position
     */
    renderBarRating: (scores, element, locale, position = 'afterbegin') => {
        for (const s of scores) {
            let {score, title, max_score, contents} = s;

            let barWidth = utils.calculatePercentage(max_score, score);
            let barTitle = contents[locale] && contents[locale].length
                ? contents[locale]
                : title;

            let values = {score, title: barTitle, barWidth};

            dom.appendComponent(
                element,
                position,
                View.parseBarRatingComponent(values)
            );
        }
    },

    /**
     * Parses star rating template
     * @param {Object} values
     * @return {String} parsed html
     */
    parseStarComponent: (values) => {
        let {star} = values;
        let starElements = ('<span> </span>').repeat(star);
        let templateVariables = {...values, starElements};

        return dom.templateEngine(starRatingComponent, templateVariables);
    },

    /**
     * Responsible for displaying star rating
     * @param {Array} groupedReviewsByStar
     * @param {String} element
     * @param {Number} ratingScale
     * @param {Number} totalReviewsNumber
     * @param {Boolean} displayReviews
     * @param {String} position
     */
    renderStarRating: (
        groupedReviewsByStar,
        element,
        ratingScale,
        totalReviewsNumber,
        displayReviews,
        position = 'afterbegin'
    ) => {
        let counter = 1;

        while (counter <= ratingScale) {
            let star = counter;
            let totalCountPerStar = groupedReviewsByStar[star]
                ? groupedReviewsByStar[star].length
                : '0';

            let disabled = totalCountPerStar === 0 || !displayReviews
                ? 'disabled'
                : 'default';

            let barWidth = utils.calculatePercentage(
                totalReviewsNumber,
                totalCountPerStar
            );

            let values = {star, totalCountPerStar, barWidth, disabled};

            dom.appendComponent(
                element,
                position,
                View.parseStarComponent(values)
            );

            counter++;
        }
    },

    /**
     * Parses reviews template
     * @param {Object} templateVariables
     * @return {String} parsed html
     */
    parseReviewsComponent: (templateVariables) => {
        return dom.templateEngine(reviewComponent, templateVariables);
    },

    /**
     * Responsible for displaying reviews
     * @param {Array} reviews
     * @param {Node} element
     * @param {Number} maxScore
     * @param {String} locale
     */
    renderReviews: (
        reviews,
        element,
        maxScore,
        locale
    ) => {
        let component = '';
        for (const review of reviews) {
            let values = {
                ...review,
                max_score: maxScore,
                date: utils.getDate(review.time_created, locale)
            };

            let {comment_negative, comment_positive} = values;
            if (!comment_negative || !comment_negative.length) {
                values.comment_negative_class = View.classes.mutedComment;
                values.comment_negative = translate.get(locale, 'noComment');
            }

            if (!comment_positive || !comment_positive.length) {
                values.comment_positive_class = View.classes.mutedComment;
                values.comment_positive = translate.get(locale, 'noComment');
            }

            component += View.parseReviewsComponent(values);
        }

        element.innerHTML = component;

    },

    highlightStarFilter: (starElem, reset = false) => {
        dom.removeClassFromList(
            starElem.parentElement.children,
            View.classes.highlighted
        );

        if (reset) {
            dom.toggleClass(starElem.parentElement, View.classes.muted);
            return;
        }

        dom.addClass(starElem.parentElement, View.classes.muted);
        dom.toggleClass(starElem, View.classes.highlighted);
    },

    isHighlighted: (starElem) => {
        return starElem.classList.contains(View.classes.highlighted);
    }
};

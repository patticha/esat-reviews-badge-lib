# e-satisfaction Reviews Badge

Product Review Badge is to display statistical data from the Product Review Questionnaires on the Retailer’s website.

[![codecov](https://codecov.io/bb/esatisfaction/esat-reviews-badge/branch/v1.0/graph/badge.svg?token=TB1ynaGqbg)](https://codecov.io/bb/esatisfaction/esat-reviews-badge)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 


### Installing

A step by step series, how to get a development env running.

Clone the repo locally

```
git clone git@bitbucket.org:patticha/esat-reviews-badge.git
```

Install packages & dependencies

```
npm install
```


### Running Project 

#### Modify Hosts

In order to run the webpack server locally, you need to add in `/hosts` the following: 

* `127.0.0.1 reviews.e-satisfaction.localhost`

* `127.0.0.1 cdn.e-satisfaction.localhost`

#### Bundle assets
The first step is to bundle all the assets. 
```
npm run build:dev
```
After running this command a `dist` folder, containing all the bundles, will be created.

#### Start local server
Next, you need to run the webpack-server

```
npm run start:dev
```
> The port **8080** should be free

The project will run http://reviews.e-satisfaction.localhost:8080/ 
if you need run the project in the different host or port, replace the params `--host` and `--port` in the `package.json`

> **_NOTE:_** Be aware, by changing the port you need to modify the port in paths.js

Now, you should be able to access the project by following the urls below:

1. http://reviews.e-satisfaction.localhost:8080/demo/light.html
2. http://reviews.e-satisfaction.localhost:8080/demo/dark.html
3. http://reviews.e-satisfaction.localhost:8080/demo/noreviews.html


### Development

Webpack can watch files and recompile whenever they change

```
npm run watch
```

#### Check code style errors

ESlint checks style based on the rules defined in  .eslintrc.yaml
```
npm run lint
```
> **_NOTE:_** Make sure you have eslint installed as a global package (npm -i g eslint)


## Deployment

```
npm run prod
```

The bundler will create the folder `dist` which contains the minified integration files. 

## How to debug production bundle
By default, webpack minifies the bundle when the `mode` is `production`. 

In order to produce the unminified production bundle run the following: 

```
npm run prod:debug
```

###### This commands runs the production script with the exception of passing the variable `NODE_MODE=debug`

## How deployment works

First, by passing the variable `NODE_ENV=production` we determine that the bundle folder will be distributed in production. 
Based on the `NODE_ENV` variable we also determine the following:

1. Host
2. Entry point 
3. Output 

The available host urls are defined in the `paths.js` file. So according to the urls a global variable named `HOST ` will be defined and passed to the application in order to be consumed by all the API calls (see Fetch.js). 

The `entry point` that indicates which module should use to begin building the dependency graph. 

and the `output` is where to emit the bundles it creates and how to name these files. in case of production it's `js/integration.min.js`.

## Run Test 
Unit tests written with [Jest](https://jestjs.io/)

```
npm run test
```

Watching the changes while you are writing tests

```
npm run test-watch
```
## Where API urls are defined and how to change them

As mentioned before, all host urls are defined in the `paths.js` file. 

So first you need to make sure that the  host is written there. 
 Second you need to pass the correct `NODE_ENV` variable. 

For example, if you want to run stage env, you can run `build:stage`, 
which runs `"NODE_ENV=stage webpack --config config/webpack.js"`

Last, if you want to change the backend endpoints, you can change the variable `url` at the corresponding function in the `Fetch.js` file. 

## How to add new translations
Translated literals live in the file `locale.json`. If you want to add new translation, you simply add the locale as a new property name. 

```
  "it": {
    "from":"translated literal",
  }
```

For the literals that need to be translated, you need to define them in the html by adding the data attribute translate and the key, like the example 

``` <p data-translate="from"> <p>```

## Known issues

1) you will probably end up having CORS issues when running the project locally. A solution will be to install browser extension that disables CORS.
Be aware that usually Chrome is more strict, when it continues to complain use Firefox or Safari. 

## Built With

* [Axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
* [Webpack 4 ](https://webpack.js.org/) - A bundler for javascript and css


## Dependencies 

```
├── axios@0.19.0
```
## Dev Dependencies

```
├── @babel/core@7.7.2
├── @babel/preset-env@7.7.1
├── axios@0.19.0
├── babel-jest@24.9.0
├── babel-polyfill@6.26.0
├── clean-webpack-plugin@3.0.0
├── cross-env@6.0.3
├── css-loader@3.2.0
├── eslint@6.4.0
├── fibers@>= 3.1.0
├── file-loader@4.2.0
├── html-loader@0.5.5
├── jest@24.9.0
├── node-sass@4.12.0
├── sass@^1.3.0
├── sass-loader@8.0.0
├── style-loader@1.0.0
├── url-loader@2.2.0
├── webpack@4.40.2
├── webpack-cli@3.3.9
└── webpack-dev-server@3.8.1
```

